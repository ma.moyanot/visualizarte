var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="319" deviceHeight="482">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1602778838531.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1602778838531-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1602778838531-ie8.css" /><![endif]-->\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
\
    <div id="s-d16b5473-1936-4e44-ae14-98dd7ee54f24" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Invitado_Detalle" width="319" height="482">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/d16b5473-1936-4e44-ae14-98dd7ee54f24-1602778838531.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/screens/d16b5473-1936-4e44-ae14-98dd7ee54f24-1602778838531-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/screens/d16b5473-1936-4e44-ae14-98dd7ee54f24-1602778838531-ie8.css" /><![endif]-->\
      <div class="freeLayout">\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Data-grid-3" datasizewidth="1024.0px" datasizeheight="810.0px" >\
        <div id="s-Rectangle_1" class="pie rectangle manualfit firer commentable non-processed" customid="Bg"   datasizewidth="318.0px" datasizeheight="428.0px" datasizewidthpx="317.9999999999999" datasizeheightpx="428.0" dataX="2.0" dataY="11.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_1_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_2" class="pie rectangle manualfit firer commentable non-processed" customid="Rectangle_1"   datasizewidth="300.0px" datasizeheight="393.0px" datasizewidthpx="299.9999999999999" datasizeheightpx="393.00000000000006" dataX="10.0" dataY="32.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_2_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="shapewrapper-s-Ellipse_1" customid="Ellipse_1" class="shapewrapper shapewrapper-s-Ellipse_1 non-processed"   datasizewidth="54.0px" datasizeheight="54.0px" datasizewidthpx="54.000000000000114" datasizeheightpx="54.0" dataX="38.0" dataY="64.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_1" class="svgContainer" style="width:100%; height:100%;">\
                <g>\
                    <g clip-path="url(#clip-s-Ellipse_1)">\
                            <ellipse id="s-Ellipse_1" class="pie ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse_1" cx="27.000000000000057" cy="27.0" rx="27.000000000000057" ry="27.0">\
                            </ellipse>\
                    </g>\
                </g>\
                <defs>\
                    <clipPath id="clip-s-Ellipse_1" class="clipPath">\
                            <ellipse cx="27.000000000000057" cy="27.0" rx="27.000000000000057" ry="27.0">\
                            </ellipse>\
                    </clipPath>\
                </defs>\
            </svg>\
            <div class="paddingLayer">\
                <div id="shapert-s-Ellipse_1" class="content firer" >\
                    <div class="valign">\
                        <span id="rtr-s-Ellipse_1_0"></span>\
                    </div>\
                </div>\
            </div>\
        </div>\
        <div id="s-Paragraph_1" class="pie richtext manualfit firer ie-background commentable non-processed" customid="Title"   datasizewidth="191.0px" datasizeheight="33.0px" dataX="103.0" dataY="64.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_1_0">&lt;Nombre&gt;</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_2" class="pie richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_6"   datasizewidth="171.0px" datasizeheight="24.0px" dataX="103.0" dataY="94.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_2_0">&lt;Descripci&oacute;n invitado&gt;</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="shapewrapper-s-Line_1" customid="Line 1" class="shapewrapper shapewrapper-s-Line_1 non-processed"   datasizewidth="250.0px" datasizeheight="1.0px" datasizewidthpx="250.0" datasizeheightpx="1.0" dataX="35.0" dataY="148.0" >\
            <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Line_1" class="svgContainer" style="width:100%;height:100%;">\
                <g>\
                    <g>\
                        <path id="s-Line_1" class="pie line shape non-processed-shape firer ie-background commentable non-processed" customid="Line 1" d="M 0.0 0.5 L 250.0 0.5"  >\
                        </path>\
                    </g>\
                </g>\
                <defs>\
                </defs>\
            </svg>\
        </div>\
        <div id="s-Paragraph_3" class="pie richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_7"   datasizewidth="202.0px" datasizeheight="40.0px" dataX="38.0" dataY="173.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_3_0">&lt;Biograf&iacute;a&gt;<br /></span><span id="rtr-s-Paragraph_3_1">Lorem ipsum dolor sit amet</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_4" class="pie richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_8"   datasizewidth="202.0px" datasizeheight="40.0px" dataX="38.0" dataY="240.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_4_0">&lt;Texto dram&aacute;tico&gt;<br /></span><span id="rtr-s-Paragraph_4_1">Lorem ipsum dolor sit amet</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_5" class="pie richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_9"   datasizewidth="202.0px" datasizeheight="40.0px" dataX="38.0" dataY="307.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_5_0">&lt;Entrevistas&gt;<br /></span><span id="rtr-s-Paragraph_5_1">Lorem ipsum dolor sit amet</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_6" class="pie richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_9"   datasizewidth="202.0px" datasizeheight="40.0px" dataX="38.0" dataY="373.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_6_0">&lt;Fotos&gt;<br /></span><span id="rtr-s-Paragraph_6_1">Lorem ipsum dolor sit amet</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_1" class="pie image firer ie-background commentable non-processed" customid="Image_9"   datasizewidth="28.0px" datasizeheight="26.0px" dataX="217.0" dataY="178.0"   alt="image" systemName="./images/98c3ca72-f773-4e95-864d-466f0c291d81.svg" overlay="#A35041">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="68px" version="1.1" viewBox="0 0 67 68" width="67px">\
          	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
          	    <title>Stroked Arrow Right</title>\
          	    <desc>Created with Sketch.</desc>\
          	    <defs />\
          	    <g fill="none" fill-rule="evenodd" id="s-Image_1-Page-1" stroke="none" stroke-width="1">\
          	        <g id="s-Image_1-Components" transform="translate(-189.000000, -1562.000000)">\
          	            <g id="s-Image_1-Arrows" transform="translate(100.000000, 1563.000000)">\
          	                <g id="s-Image_1-Stroked-Arrow-Right" transform="translate(89.000000, 0.000000)">\
          	                    <circle cx="33.5" cy="33" id="s-Image_1-Stroke" r="33" stroke="#EEEEEE" style="stroke:#A35041 !important;" />\
          	                    <g fill="#DDDDDD" id="s-Image_1-Arrow-Right" transform="translate(28.000000, 18.000000)">\
          	                        <polyline points="1.157 0 0 1.144 12.325 13.62 0 25.691 1.325 27 15 13.691 1.157 0" style="fill:#A35041 !important;" />\
          	                    </g>\
          	                </g>\
          	            </g>\
          	        </g>\
          	    </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_2" class="pie image firer ie-background commentable non-processed" customid="Image_9"   datasizewidth="28.0px" datasizeheight="26.0px" dataX="217.0" dataY="241.0"   alt="image" systemName="./images/8baecfe4-06a0-48ec-8653-7c754e313eb0.svg" overlay="#A35041">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="68px" version="1.1" viewBox="0 0 67 68" width="67px">\
          	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
          	    <title>Stroked Arrow Right</title>\
          	    <desc>Created with Sketch.</desc>\
          	    <defs />\
          	    <g fill="none" fill-rule="evenodd" id="s-Image_2-Page-1" stroke="none" stroke-width="1">\
          	        <g id="s-Image_2-Components" transform="translate(-189.000000, -1562.000000)">\
          	            <g id="s-Image_2-Arrows" transform="translate(100.000000, 1563.000000)">\
          	                <g id="s-Image_2-Stroked-Arrow-Right" transform="translate(89.000000, 0.000000)">\
          	                    <circle cx="33.5" cy="33" id="s-Image_2-Stroke" r="33" stroke="#EEEEEE" style="stroke:#A35041 !important;" />\
          	                    <g fill="#DDDDDD" id="s-Image_2-Arrow-Right" transform="translate(28.000000, 18.000000)">\
          	                        <polyline points="1.157 0 0 1.144 12.325 13.62 0 25.691 1.325 27 15 13.691 1.157 0" style="fill:#A35041 !important;" />\
          	                    </g>\
          	                </g>\
          	            </g>\
          	        </g>\
          	    </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_3" class="pie image firer ie-background commentable non-processed" customid="Image_9"   datasizewidth="28.0px" datasizeheight="26.0px" dataX="217.0" dataY="309.0"   alt="image" systemName="./images/f3658396-21c8-4f75-9d3b-9f3aa9b969e6.svg" overlay="#A35041">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="68px" version="1.1" viewBox="0 0 67 68" width="67px">\
          	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
          	    <title>Stroked Arrow Right</title>\
          	    <desc>Created with Sketch.</desc>\
          	    <defs />\
          	    <g fill="none" fill-rule="evenodd" id="s-Image_3-Page-1" stroke="none" stroke-width="1">\
          	        <g id="s-Image_3-Components" transform="translate(-189.000000, -1562.000000)">\
          	            <g id="s-Image_3-Arrows" transform="translate(100.000000, 1563.000000)">\
          	                <g id="s-Image_3-Stroked-Arrow-Right" transform="translate(89.000000, 0.000000)">\
          	                    <circle cx="33.5" cy="33" id="s-Image_3-Stroke" r="33" stroke="#EEEEEE" style="stroke:#A35041 !important;" />\
          	                    <g fill="#DDDDDD" id="s-Image_3-Arrow-Right" transform="translate(28.000000, 18.000000)">\
          	                        <polyline points="1.157 0 0 1.144 12.325 13.62 0 25.691 1.325 27 15 13.691 1.157 0" style="fill:#A35041 !important;" />\
          	                    </g>\
          	                </g>\
          	            </g>\
          	        </g>\
          	    </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_4" class="pie image firer ie-background commentable non-processed" customid="Image_9"   datasizewidth="28.0px" datasizeheight="26.0px" dataX="217.0" dataY="375.0"   alt="image" systemName="./images/31852dbe-c792-4eab-879a-e6d160d462ca.svg" overlay="#A35041">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="68px" version="1.1" viewBox="0 0 67 68" width="67px">\
          	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
          	    <title>Stroked Arrow Right</title>\
          	    <desc>Created with Sketch.</desc>\
          	    <defs />\
          	    <g fill="none" fill-rule="evenodd" id="s-Image_4-Page-1" stroke="none" stroke-width="1">\
          	        <g id="s-Image_4-Components" transform="translate(-189.000000, -1562.000000)">\
          	            <g id="s-Image_4-Arrows" transform="translate(100.000000, 1563.000000)">\
          	                <g id="s-Image_4-Stroked-Arrow-Right" transform="translate(89.000000, 0.000000)">\
          	                    <circle cx="33.5" cy="33" id="s-Image_4-Stroke" r="33" stroke="#EEEEEE" style="stroke:#A35041 !important;" />\
          	                    <g fill="#DDDDDD" id="s-Image_4-Arrow-Right" transform="translate(28.000000, 18.000000)">\
          	                        <polyline points="1.157 0 0 1.144 12.325 13.62 0 25.691 1.325 27 15 13.691 1.157 0" style="fill:#A35041 !important;" />\
          	                    </g>\
          	                </g>\
          	            </g>\
          	        </g>\
          	    </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_5" class="pie image firer click ie-background commentable non-processed" customid="Image_18"   datasizewidth="20.0px" datasizeheight="20.0px" dataX="290.0" dataY="5.0"   alt="image" systemName="./images/deb09f33-131c-450e-a5df-e84ed6e38611.svg" overlay="#A35041">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="20px" version="1.1" viewBox="0 0 20 20" width="20px">\
          	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
          	    <title>close-icon copy</title>\
          	    <desc>Created with Sketch.</desc>\
          	    <defs />\
          	    <g fill="none" fill-rule="evenodd" id="s-Image_5-Page-1" stroke="none" stroke-width="1">\
          	        <g fill="#B2B2B2" id="s-Image_5-Components" transform="translate(-729.000000, -1389.000000)">\
          	            <g id="s-Image_5-close-icon-copy" transform="translate(729.000000, 1389.000000)">\
          	                <path d="M11.768777,10.0002085 L19.6335286,2.13512894 C20.1221571,1.64689695 20.1221571,0.854718672 19.6335286,0.366486689 C19.1449,-0.12216223 18.3540055,-0.12216223 17.8649601,0.366486689 L10.0002085,8.23156622 L2.13545684,0.366486689 C1.64682829,-0.12216223 0.855099956,-0.12216223 0.36647141,0.366486689 C-0.122157137,0.854718672 -0.122157137,1.64689695 0.36647141,2.13512894 L8.23205687,10.0002085 L0.36647141,17.865288 C-0.122157137,18.3539369 -0.122157137,19.1456983 0.36647141,19.6339302 C0.610785683,19.8778378 0.930979133,20 1.25075566,20 C1.57094912,20 1.89114257,19.8778378 2.13545684,19.6335133 L10.0002085,11.7684338 L17.8649601,19.6335133 C18.1092744,19.8778378 18.4298847,20 18.7492443,20 C19.0694378,20 19.3892143,19.8778378 19.6335286,19.6335133 C20.1221571,19.1452813 20.1221571,18.353103 19.6335286,17.8648711 L11.768777,10.0002085 Z" id="s-Image_5-Fill-1" style="fill:#A35041 !important;" />\
          	            </g>\
          	        </g>\
          	    </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;