var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1280" deviceHeight="800">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1602778838531.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1602778838531-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><![endif]-->\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
\
    <div id="s-d12245cc-1680-458d-89dd-4f0d7fb22724" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="INDEX" width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/d12245cc-1680-458d-89dd-4f0d7fb22724-1602778838531.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/screens/d12245cc-1680-458d-89dd-4f0d7fb22724-1602778838531-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/screens/d12245cc-1680-458d-89dd-4f0d7fb22724-1602778838531-ie8.css" /><![endif]-->\
      <div class="freeLayout">\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Group_1" datasizewidth="1024.0px" datasizeheight="600.0px" >\
        <div id="s-Rectangle_1" class="pie rectangle manualfit firer commentable non-processed" customid="Rectangle_1"   datasizewidth="1280.0px" datasizeheight="651.0px" datasizewidthpx="1280.0000000000002" datasizeheightpx="651.0" dataX="0.0" dataY="0.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_1_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_1" class="pie richtext autofit firer mousedown mouseup ie-background commentable non-processed" customid="Text_1"   datasizewidth="65.9px" datasizeheight="19.0px" dataX="865.9" dataY="43.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_1_0">INVITADOS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_3" class="pie richtext autofit firer mousedown mouseup ie-background commentable non-processed" customid="Text_3"   datasizewidth="58.2px" datasizeheight="55.0px" dataX="959.0" dataY="44.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_3_0">QUI&Eacute;NES <br />SOMOS<br /><br /></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_4" class="pie richtext autofit firer mousedown mouseup ie-background commentable non-processed" customid="Text_4"   datasizewidth="68.5px" datasizeheight="19.0px" dataX="1041.0" dataY="44.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_4_0">CONTACTO</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Image_1" class="pie image lockV firer ie-background commentable non-processed" customid="Image_2"   datasizewidth="31.3px" datasizeheight="22.5px" dataX="1191.0" dataY="43.0" aspectRatio="0.72"   alt="image" systemName="./images/e12a046a-86b9-4ad4-bda1-bcd2730df2ab.svg" overlay="#66A5AA">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="18px" version="1.1" viewBox="0 0 25 18" width="25px">\
            	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
            	    <title>Menu Burger Icon</title>\
            	    <desc>Created with Sketch.</desc>\
            	    <defs />\
            	    <g fill="none" fill-rule="evenodd" id="s-Image_1-Page-1" stroke="none" stroke-width="1">\
            	        <g fill="#333333" id="Header-#2" transform="translate(-120.000000, -26.000000)">\
            	            <g id="s-Image_1-Top">\
            	                <path d="M145,26 L145,28 L120,28 L120,26 L145,26 Z M145,42 L145,44 L120,44 L120,42 L145,42 Z M145,34 L145,36 L120,36 L120,34 L145,34 Z" id="s-Image_1-Menu-Burger-Icon" style="fill:#66A5AA !important;" />\
            	            </g>\
            	        </g>\
            	    </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_5" class="pie richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_2"   datasizewidth="75.0px" datasizeheight="55.0px" dataX="602.5" dataY="651.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_5_0">Desplazarse hacia abajo</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Image_2" class="pie image lockV firer click ie-background commentable non-processed" customid="Image_3"   datasizewidth="22.5px" datasizeheight="22.5px" dataX="628.8" dataY="628.5" aspectRatio="1.0"   alt="image" systemName="./images/664f8172-5fc1-48e3-a8ff-6985c485ff4f.svg" overlay="#A35041">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="18px" version="1.1" viewBox="0 0 18 18" width="18px">\
            	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
            	    <title>arrow</title>\
            	    <desc>Created with Sketch.</desc>\
            	    <defs />\
            	    <g fill="none" fill-rule="evenodd" id="s-Image_2-Page-1" stroke="none" stroke-width="1">\
            	        <g id="Header-#9" transform="translate(-589.000000, -505.000000)">\
            	            <g id="s-Image_2-arrow" transform="translate(590.000000, 506.000000)">\
            	                <g id="s-Image_2-2" transform="translate(8.000000, 8.000000) scale(-1, -1) translate(-8.000000, -8.000000) ">\
            	                    <circle cx="8" cy="8" id="s-Image_2-Oval-4" r="8" stroke="#979797" style="stroke:#A35041 !important;" />\
            	                    <path d="M10.1350279,7.47556953 C10.1222119,7.48773525 10.1035705,7.48434017 10.0892982,7.49339373 L6.61704757,10.5877309 C6.5084032,10.6929786 6.33276633,10.6929786 6.22441324,10.5877309 C6.11606014,10.4824833 6.11606014,10.3115973 6.22441324,10.2063497 L9.51607537,7.27356197 L6.22412196,4.34077423 C6.11576887,4.23552659 6.11576887,4.06464063 6.22412196,3.95967592 C6.33247506,3.8547112 6.50811193,3.8547112 6.61646502,3.95967592 L10.0881331,7.05259851 C10.1026967,7.06221791 10.1222119,7.05882283 10.1350279,7.0715544 C10.1926997,7.12729038 10.2174578,7.20141641 10.2139625,7.27469366 C10.2168752,7.34683922 10.1915346,7.42011648 10.1350279,7.47556953 Z" fill="#979797" id="s-Image_2-Shape" transform="translate(8.178571, 7.273810) rotate(270.000000) translate(-8.178571, -7.273810) " style="fill:#A35041 !important;" />\
            	                </g>\
            	            </g>\
            	        </g>\
            	    </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_6" class="pie richtext autofit firer mousedown mouseup ie-background commentable non-processed" customid="Text_1"   datasizewidth="56.5px" datasizeheight="37.0px" dataX="790.0" dataY="43.5" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_6_0">EVENTOS<br /><br /></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_3" class="pie image firer click ie-background commentable non-processed" customid="Image 1"   datasizewidth="61.0px" datasizeheight="63.0px" dataX="31.0" dataY="12.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/c8f2839d-0224-4723-83fc-71853716e02d.png" />\
        	</div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Group_1" datasizewidth="1024.0px" datasizeheight="540.0px" >\
        <div id="s-Rectangle_2" class="pie rectangle manualfit firer commentable non-processed" customid="Rectangle_1"   datasizewidth="1021.0px" datasizeheight="391.0px" datasizewidthpx="1020.9999999999995" datasizeheightpx="390.9999999999995" dataX="130.0" dataY="130.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_2_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="shapewrapper-s-Ellipse_1" customid="Ellipse_1" class="shapewrapper shapewrapper-s-Ellipse_1 non-processed"   datasizewidth="10.0px" datasizeheight="10.0px" datasizewidthpx="10.0" datasizeheightpx="10.0" dataX="635.0" dataY="495.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_1" class="svgContainer" style="width:100%; height:100%;">\
                <g>\
                    <g clip-path="url(#clip-s-Ellipse_1)">\
                            <ellipse id="s-Ellipse_1" class="pie ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse_1" cx="5.0" cy="5.0" rx="5.0" ry="5.0">\
                            </ellipse>\
                    </g>\
                </g>\
                <defs>\
                    <clipPath id="clip-s-Ellipse_1" class="clipPath">\
                            <ellipse cx="5.0" cy="5.0" rx="5.0" ry="5.0">\
                            </ellipse>\
                    </clipPath>\
                </defs>\
            </svg>\
            <div class="paddingLayer">\
                <div id="shapert-s-Ellipse_1" class="content firer" >\
                    <div class="valign">\
                        <span id="rtr-s-Ellipse_1_0"></span>\
                    </div>\
                </div>\
            </div>\
        </div>\
        <div id="shapewrapper-s-Ellipse_2" customid="Ellipse_2" class="shapewrapper shapewrapper-s-Ellipse_2 non-processed"   datasizewidth="10.0px" datasizeheight="10.0px" datasizewidthpx="10.0" datasizeheightpx="10.0" dataX="655.0" dataY="495.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_2" class="svgContainer" style="width:100%; height:100%;">\
                <g>\
                    <g clip-path="url(#clip-s-Ellipse_2)">\
                            <ellipse id="s-Ellipse_2" class="pie ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse_2" cx="5.0" cy="5.0" rx="5.0" ry="5.0">\
                            </ellipse>\
                    </g>\
                </g>\
                <defs>\
                    <clipPath id="clip-s-Ellipse_2" class="clipPath">\
                            <ellipse cx="5.0" cy="5.0" rx="5.0" ry="5.0">\
                            </ellipse>\
                    </clipPath>\
                </defs>\
            </svg>\
            <div class="paddingLayer">\
                <div id="shapert-s-Ellipse_2" class="content firer" >\
                    <div class="valign">\
                        <span id="rtr-s-Ellipse_2_0"></span>\
                    </div>\
                </div>\
            </div>\
        </div>\
        <div id="shapewrapper-s-Ellipse_3" customid="Ellipse_3" class="shapewrapper shapewrapper-s-Ellipse_3 non-processed"   datasizewidth="10.0px" datasizeheight="10.0px" datasizewidthpx="10.0" datasizeheightpx="10.0" dataX="616.0" dataY="495.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_3" class="svgContainer" style="width:100%; height:100%;">\
                <g>\
                    <g clip-path="url(#clip-s-Ellipse_3)">\
                            <ellipse id="s-Ellipse_3" class="pie ellipse shape non-processed-shape manualfit firer ie-background commentable non-processed" customid="Ellipse_3" cx="5.0" cy="5.0" rx="5.0" ry="5.0">\
                            </ellipse>\
                    </g>\
                </g>\
                <defs>\
                    <clipPath id="clip-s-Ellipse_3" class="clipPath">\
                            <ellipse cx="5.0" cy="5.0" rx="5.0" ry="5.0">\
                            </ellipse>\
                    </clipPath>\
                </defs>\
            </svg>\
            <div class="paddingLayer">\
                <div id="shapert-s-Ellipse_3" class="content firer" >\
                    <div class="valign">\
                        <span id="rtr-s-Ellipse_3_0"></span>\
                    </div>\
                </div>\
            </div>\
        </div>\
        <div id="s-Paragraph_7" class="pie richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_1"   datasizewidth="382.0px" datasizeheight="85.0px" dataX="449.0" dataY="308.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_7_0">&lt;Descripci&oacute;n del evento&gt;<br />&lt;Fecha y hora del evento&gt;<br />&lt;Ubicaci&oacute;n del evento&gt;</span><span id="rtr-s-Paragraph_7_1"> </span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Image_4" class="pie image lockV firer ie-background commentable non-processed" customid="Image_2"   datasizewidth="68.0px" datasizeheight="68.0px" dataX="181.0" dataY="257.0" aspectRatio="1.0"   alt="image" systemName="./images/1081be67-a00f-44cd-b054-1625d286947d.svg" overlay="">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg preserveAspectRatio=\'none\' width="68px" height="68px" viewBox="0 0 68 68" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
            	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
            	    <title>Stroked Arrow Left</title>\
            	    <desc>Created with Sketch.</desc>\
            	    <defs></defs>\
            	    <g id="s-Image_4-Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
            	        <g id="Slider-#9" transform="translate(-98.000000, -243.000000)">\
            	            <g id="s-Image_4-Stroked-Arrow-Left" transform="translate(99.000000, 244.000000)">\
            	                <circle id="s-Image_4-Stroke" stroke="#CBCBCB" cx="33" cy="33" r="33"></circle>\
            	                <g id="s-Image_4-Arrow-Left" transform="translate(23.000000, 19.000000)" fill="#CBCBCB">\
            	                    <polyline transform="translate(8.000000, 14.000000) scale(-1, 1) translate(-8.000000, -14.000000) " points="1.157 0 0 1.144 12.325 13.62 0 25.691 1.325 27 15 13.691 1.157 0"></polyline>\
            	                </g>\
            	            </g>\
            	        </g>\
            	    </g>\
            	</svg>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Image_5" class="pie image lockV firer click ie-background commentable non-processed" customid="Image_3"   datasizewidth="68.0px" datasizeheight="68.0px" dataX="1025.0" dataY="257.0" aspectRatio="1.0"  rotationdeg="180" alt="image" systemName="./images/e637ec62-321a-495c-bc51-4e07927d3b13.svg" overlay="">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg preserveAspectRatio=\'none\' width="68px" height="68px" viewBox="0 0 68 68" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
            	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
            	    <title>Stroked Arrow Left</title>\
            	    <desc>Created with Sketch.</desc>\
            	    <defs></defs>\
            	    <g id="s-Image_5-Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
            	        <g id="Slider-#9" transform="translate(-98.000000, -243.000000)">\
            	            <g id="s-Image_5-Stroked-Arrow-Left" transform="translate(99.000000, 244.000000)">\
            	                <circle id="s-Image_5-Stroke" stroke="#CBCBCB" cx="33" cy="33" r="33"></circle>\
            	                <g id="s-Image_5-Arrow-Left" transform="translate(23.000000, 19.000000)" fill="#CBCBCB">\
            	                    <polyline transform="translate(8.000000, 14.000000) scale(-1, 1) translate(-8.000000, -14.000000) " points="1.157 0 0 1.144 12.325 13.62 0 25.691 1.325 27 15 13.691 1.157 0"></polyline>\
            	                </g>\
            	            </g>\
            	        </g>\
            	    </g>\
            	</svg>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Rectangle_3" class="pie rectangle manualfit firer mouseenter mouseleave mousedown mouseup commentable non-processed" customid="Rectangle_2"   datasizewidth="160.0px" datasizeheight="43.0px" datasizewidthpx="160.0" datasizeheightpx="43.0" dataX="560.0" dataY="393.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_3_0">M&Aacute;S INFORMACI&Oacute;N</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_8" class="pie richtext autofit firer ie-background commentable non-processed" customid="Text_1"   datasizewidth="353.8px" datasizeheight="101.0px" dataX="463.6" dataY="240.5" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_8_0">&lt;Pr&oacute;ximos eventos&gt;<br /><br /></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;