(function(window, undefined) {
  var dictionary = {
    "d16b5473-1936-4e44-ae14-98dd7ee54f24": "Invitado_Detalle",
    "271b2a67-3603-452f-8b11-c20bef3b6b2c": "Eventos",
    "8fa8304a-f6b1-4124-b578-10d4b648c78f": "Contacto",
    "9db4a893-b33b-47a9-a39d-360c6b3cda1d": "Invitados",
    "3c5ae445-77de-49c2-a386-84b2600623a7": "Quienes somos",
    "9bec1a32-f3b7-4132-a3e7-80f8bffaef20": "Index_2",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "INDEX",
    "4065ced1-beab-43e1-96db-6312f1eed020": "Invitados_2",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);